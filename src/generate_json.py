import os
import json

from PIL import Image
import PIL
import os
import glob

def convert_image(image_path, result_path, image_type, index, custom_size=1000):
    # 1. Opening the image:
    im = Image.open(image_path)
    im = im.convert('RGB')
    image_name = image_path.split('.')[0]
    print(f"This is the image name: {image_name}")
    current_path = os.getcwd()
    folder_name = os.path.basename(current_path)
    if image_type == 'jpg' or image_type == 'png' or image_name[0] != '.':
        if im.size[0] > 600:
            im.thumbnail(size=((custom_size, custom_size)))
            im.save(f"{result_path}/{index}.webp", 'webp')
            # os.remove(image_path)
        else:
            im.save(f"{result_path}/{index}.webp", 'webp')
            # os.remove(image_path)
    else:
        # Raising an error if we didn't get a jpeg or png file type!
        raise Error




# [convert_image(image, image_type='jpg', custom_size=1000) if image.endswith('jpg') else
# convert_image(image, image_type='png', custom_size=1000) for image in images]

res = {}

for folder in os.listdir('../public/images'):
    i = 0
    os.mkdir('../public/webp/'+ folder)
    if os.path.isdir and folder[0] != '.':
        for file in os.listdir('../public/images/'+ folder):
            i = i + 1
            print(file)
            if file.endswith(('jpg', 'png', 'jpeg')):
                print(file)
                convert_image('../public/images/'+ folder+"/"+file, result_path='../public/webp/'+ folder, image_type='jpg', index=i, custom_size=1000)
    # images = [file for file in folder if file.endswith(('jpg', 'png'))]
    # [convert_image(image, image_type='jpg', custom_size=1000) if image.endswith('jpg') else
    # convert_image(image, image_type='png', custom_size=1000) for image in images]
    # check if current path is a file
    arr = []
    if os.path.isdir(os.path.join('../public/images', folder)):
        for path in os.listdir('../public/images/'+ folder):
            if os.path.isfile and path[0] != '.':
                arr.append('/vite-gallery/images/'+ str(folder)+"/"+path)
        arr.sort()
        res[str(folder)] = arr
file= open('generated_json.json', 'w')
file.write(json.dumps(res))
file.close()
# print(json.dumps(res))