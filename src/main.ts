import { createApp } from "vue";
import "./style.css";
import { createPinia } from "pinia";
const pinia = createPinia();
import App from "./App.vue";
import "@mdi/font/css/materialdesignicons.css";
import { router } from "./router";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

pinia.use(piniaPluginPersistedstate);
const app = createApp(App);

app.use(router);
app.use(pinia);
app.mount("#app");
