import { defineStore } from "pinia";

export const usePhotosStore = defineStore("photos", {
  state: () => {
    return {
      favorites: [] as string[],
    };
  },
  getters: {
    getFavoriteImages: (state) => state.favorites,
  },
  actions: {
    addToFavorites(src: string) {
      this.favorites.push(src);
    },
    deleteFromFavorites(src: string) {
      this.favorites = this.favorites.filter((item) => item != src);
    },
  },
  persist: true,
});
