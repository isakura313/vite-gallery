import Home from "../pages/Home.vue";
import Gallery from "../pages/Gallery.vue";
import Random from "../pages/Random.vue";
import Favorites from "../pages/Favorites.vue";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  { path: "/", component: Home, name: "Home" },
  {
    path: "/gallery/:id",
    component: Gallery,
    name: "gallery",
  },
  {
    path: "/random",
    component: Random,
    name: "random",
  },
  {
    path: "/favorites",
    component: Favorites,
    name: "favorites",
  },
];

export const router = createRouter({
  history: createWebHistory("/vite-gallery/"),
  routes,
});
